<?php

namespace Smorken\Controller\View\WithService;

use Smorken\Controller\View\Controller;
use Smorken\Controller\View\WithService\Concerns\HasDelete;
use Smorken\Service\Contracts\Services\DeleteService;
use Smorken\Service\Contracts\Services\RetrieveService;

abstract class DeleteController extends Controller implements \Smorken\Controller\Contracts\View\DeleteController
{
    use HasDelete;

    public function __construct(protected DeleteService $deleteService, protected RetrieveService $retrieveService)
    {
        parent::__construct();
    }

    public function getDeleteService(): DeleteService
    {
        return $this->deleteService;
    }

    public function getRetrieveService(): RetrieveService
    {
        return $this->retrieveService;
    }
}
