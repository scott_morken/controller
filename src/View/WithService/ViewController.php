<?php

namespace Smorken\Controller\View\WithService;

use Smorken\Controller\View\Controller;
use Smorken\Controller\View\WithService\Concerns\HasView;
use Smorken\Service\Contracts\Services\RetrieveService;

abstract class ViewController extends Controller implements \Smorken\Controller\Contracts\View\ViewController
{
    use HasView;

    public function __construct(protected RetrieveService $retrieveService)
    {
        parent::__construct();
    }

    public function getRetrieveService(): RetrieveService
    {
        return $this->retrieveService;
    }
}
