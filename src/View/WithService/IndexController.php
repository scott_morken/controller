<?php

namespace Smorken\Controller\View\WithService;

use Smorken\Controller\View\Controller;
use Smorken\Controller\View\WithService\Concerns\HasIndex;
use Smorken\Service\Contracts\Services\IndexService;

abstract class IndexController extends Controller implements \Smorken\Controller\Contracts\View\IndexController
{
    use HasIndex;

    public function __construct(protected IndexService $indexService)
    {
        parent::__construct();
    }
}
