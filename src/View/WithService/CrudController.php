<?php

namespace Smorken\Controller\View\WithService;

use Smorken\Controller\View\Controller;
use Smorken\Controller\View\WithService\Concerns\HasCreate;
use Smorken\Controller\View\WithService\Concerns\HasDelete;
use Smorken\Controller\View\WithService\Concerns\HasIndex;
use Smorken\Controller\View\WithService\Concerns\HasUpdate;
use Smorken\Controller\View\WithService\Concerns\HasView;
use Smorken\Service\Contracts\Services\CrudServices;
use Smorken\Service\Contracts\Services\IndexService;

abstract class CrudController extends Controller
{
    use HasCreate, HasDelete, HasIndex, HasUpdate, HasView;

    public function __construct(
        CrudServices $crudServices,
        IndexService $indexService
    ) {
        $this->setCrudServices($crudServices);
        $this->setIndexService($indexService);
        parent::__construct();
    }
}
