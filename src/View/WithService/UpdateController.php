<?php

namespace Smorken\Controller\View\WithService;

use Smorken\Controller\View\Controller;
use Smorken\Controller\View\WithService\Concerns\HasUpdate;
use Smorken\Service\Contracts\Services\RetrieveService;
use Smorken\Service\Contracts\Services\UpdateService;

abstract class UpdateController extends Controller implements \Smorken\Controller\Contracts\View\UpdateController
{
    use HasUpdate;

    public function __construct(protected UpdateService $updateService, protected RetrieveService $retrieveService)
    {
        parent::__construct();
    }

    public function getRetrieveService(): RetrieveService
    {
        return $this->retrieveService;
    }

    public function getUpdateService(): UpdateService
    {
        return $this->updateService;
    }
}
