<?php

namespace Smorken\Controller\View\WithService\Concerns;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Smorken\Service\Contracts\Services\VO\ModelResult;
use Smorken\Service\Services\VO\RedirectActionResult;
use Smorken\Service\Services\VO\RedirectBackResponse;

trait HandlesSave
{
    use HasFlashMessages;

    public function doSave(Request $request, int|string|null $id = null): RedirectResponse
    {
        if ($id) {
            return $this->doUpdate($request, $id);
        }

        return $this->doCreate($request);
    }

    protected function getSaveRedirectAction(Request $request, ModelResult $result): array
    {
        return $this->actionArray('index');
    }

    protected function saveRedirectOnFailure(Request $request, ModelResult $result): RedirectResponse
    {
        $request->session()->flash('flash:danger', sprintf('Error saving resource [%s].', $result->id ?? 'new'));
        if (method_exists($this, 'onSaveFailure')) {
            $this->onSaveFailure($request, $result);
        }

        return (new RedirectBackResponse(true))->redirect();
    }

    protected function saveRedirectOnSuccess(Request $request, ModelResult $result): RedirectResponse
    {
        $request->session()->flash('flash:success', sprintf('Saved resource [%s].', $result->id));
        if (method_exists($this, 'onSaveSuccess')) {
            $this->onSaveSuccess($request, $result);
        }

        return (new RedirectActionResult($this->getSaveRedirectAction($request, $result),
            $this->getParams($request)))->redirect();
    }

    protected function saveRedirectWithResult(Request $request, ModelResult $result): RedirectResponse
    {
        $this->addMessagesToRequest($request, $result);
        if ($result->result) {
            return $this->saveRedirectOnSuccess($request, $result);
        }

        return $this->saveRedirectOnFailure($request, $result);
    }
}
