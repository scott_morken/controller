<?php

namespace Smorken\Controller\View\WithService\Concerns;

use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Smorken\Service\Contracts\Services\CreateService;

trait HasCreate
{
    use HandlesSave, HasCrudServices, HasFilter;

    public function create(Request $request): View
    {
        return $this->makeView($this->getViewName('create'))
            ->with('filter', $this->getFilter($request))
            ->with('model', $this->getCreateService()->getModel());
    }

    public function doCreate(Request $request): RedirectResponse
    {
        $result = $this->getCreateService()->createFromRequest($request);

        return $this->saveRedirectWithResult($request, $result);
    }

    public function getCreateService(): CreateService
    {
        return $this->getCrudServices()->getCreateService();
    }
}
