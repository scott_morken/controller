<?php

namespace Smorken\Controller\View\WithService\Concerns;

use Illuminate\Http\Request;
use Smorken\Service\Contracts\Services\FilterService;
use Smorken\Support\Contracts\Filter;

trait HasFilter
{
    protected function getFilter(Request $request): Filter
    {
        if ($this->hasFilterService()) {
            return $this->getFilterService()->getFilterFromRequest($request);
        }

        return $this->getFilterFromRequest($request);
    }

    protected function getFilterFromRequest(Request $request): Filter
    {
        return \Smorken\Support\Filter::fromRequest($request, array_keys($request->query()));
    }

    protected function getFilterService(): FilterService
    {
        return $this->getIndexService()->getFilterService();
    }

    protected function getParams(Request $request): array
    {
        return $this->getFilter($request)->toArray();
    }

    protected function hasFilterService(): bool
    {
        return $this->hasIndexService() && method_exists($this->getIndexService(), 'getFilterService');
    }

    protected function hasIndexService(): bool
    {
        return method_exists($this, 'getIndexService');
    }
}
