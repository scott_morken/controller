<?php

namespace Smorken\Controller\View\WithService\Concerns;

use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Smorken\Service\Contracts\Enums\RetrieveTypes;
use Smorken\Service\Contracts\Services\DeleteService;

trait HasDelete
{
    use HandlesDelete, HasCrudServices, HasRetrieveService;

    public function delete(Request $request, int|string $id): View
    {
        $result = $this->getRetrieveService()->findById($id, true, RetrieveTypes::DELETE);

        return $this->makeView($this->getViewName('delete'))
            ->with('model', $result->model)
            ->with('filter', $this->getFilter($request));
    }

    public function doDelete(Request $request, int|string $id): RedirectResponse
    {
        $result = $this->getDeleteService()->deleteFromRequest($request, $id);

        return $this->deleteRedirectWithResult($request, $result);
    }

    public function getDeleteService(): DeleteService
    {
        return $this->getCrudServices()->getDeleteService();
    }
}
