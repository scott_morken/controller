<?php

namespace Smorken\Controller\View\WithService\Concerns;

use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

trait HasView
{
    use HasCrudServices, HasFilter, HasRetrieveService;

    public function view(Request $request, int|string $id): View
    {
        $result = $this->getRetrieveService()->findById($id);

        return $this->makeView($this->getViewName('view'))
            ->with('model', $result->model)
            ->with('filter', $this->getFilter($request));
    }
}
