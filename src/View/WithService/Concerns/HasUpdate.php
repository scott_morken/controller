<?php

namespace Smorken\Controller\View\WithService\Concerns;

use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Smorken\Service\Contracts\Enums\RetrieveTypes;
use Smorken\Service\Contracts\Services\UpdateService;

trait HasUpdate
{
    use HandlesSave, HasCrudServices, HasFilter, HasRetrieveService;

    public function doUpdate(Request $request, int|string $id): RedirectResponse
    {
        $result = $this->getUpdateService()->updateFromRequest($request, $id);

        return $this->saveRedirectWithResult($request, $result);
    }

    public function getUpdateService(): UpdateService
    {
        return $this->getCrudServices()->getUpdateService();
    }

    public function update(Request $request, int|string $id): View
    {
        $result = $this->getRetrieveService()->findById($id, true, RetrieveTypes::UPDATE);

        return $this->makeView($this->getViewName('update'))
            ->with('model', $result->model)
            ->with('filter', $this->getFilter($request));
    }
}
