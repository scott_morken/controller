<?php

namespace Smorken\Controller\View\WithService\Concerns;

use Illuminate\Http\Request;
use Smorken\Service\Contracts\Services\VO\HasMessages;

trait HasFlashMessages
{
    protected function addMessagesToRequest(Request $request, HasMessages $result): void
    {
        $counter = 0;
        foreach ($result->messages as $message) {
            $key = $message->key ?? 'flash:info';
            if ($request->session()->has($key)) {
                $key = $key.':'.$counter;
                $counter++;
            }
            $request->session()->flash($key, $message->message);
        }
    }
}
