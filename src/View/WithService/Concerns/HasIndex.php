<?php

namespace Smorken\Controller\View\WithService\Concerns;

use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Smorken\Service\Contracts\Services\IndexService;

trait HasIndex
{
    protected IndexService $indexService;

    public function getIndexService(): IndexService
    {
        return $this->indexService;
    }

    public function setIndexService(IndexService $service): void
    {
        $this->indexService = $service;
    }

    public function index(Request $request): View
    {
        $view = $this->makeView($this->getViewName('index'));

        return $this->indexFromService($view, $request, $this->getIndexService());
    }

    protected function indexFromService(
        View $view,
        Request $request,
        IndexService $service
    ): View {
        $result = $service->getByRequest($request);
        if (property_exists($result, 'filter')) {
            $view->with('filter', $result->filter);
        }

        return $view->with('models', $result->models);
    }
}
