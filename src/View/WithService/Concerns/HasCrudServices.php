<?php

namespace Smorken\Controller\View\WithService\Concerns;

use Smorken\Service\Contracts\Services\CrudServices;

trait HasCrudServices
{
    protected CrudServices $crudServices;

    public function getCrudServices(): CrudServices
    {
        return $this->crudServices;
    }

    public function setCrudServices(CrudServices $service): void
    {
        $this->crudServices = $service;
    }
}
