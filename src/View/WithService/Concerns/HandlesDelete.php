<?php

namespace Smorken\Controller\View\WithService\Concerns;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Smorken\Service\Contracts\Services\VO\ModelResult;
use Smorken\Service\Services\VO\RedirectActionResult;

trait HandlesDelete
{
    use HasFilter, HasFlashMessages;

    protected function deleteRedirectOnFailure(Request $request, ModelResult $result): RedirectResponse
    {
        $request->session()->flash('flash:danger', sprintf('Error deleting resource [%s].', $result->id ?? 'new'));
        if (method_exists($this, 'onDeleteFailure')) {
            $this->onDeleteFailure($request, $result);
        }

        return (new RedirectActionResult($this->getDeleteRedirectAction($request, $result),
            $this->getParams($request)))->redirect();
    }

    protected function deleteRedirectOnSuccess(Request $request, ModelResult $result): RedirectResponse
    {
        $request->session()->flash('flash:success', sprintf('Deleted resource [%s].', $result->id));
        if (method_exists($this, 'onDeleteSuccess')) {
            $this->onDeleteSuccess($request, $result);
        }

        return (new RedirectActionResult($this->getDeleteRedirectAction($request, $result),
            $this->getParams($request)))->redirect();
    }

    protected function deleteRedirectWithResult(Request $request, ModelResult $result): RedirectResponse
    {
        $this->addMessagesToRequest($request, $result);
        if ($result->result) {
            return $this->deleteRedirectOnSuccess($request, $result);
        }

        return $this->deleteRedirectOnFailure($request, $result);
    }

    protected function getDeleteRedirectAction(Request $request, ModelResult $result): array
    {
        return $this->actionArray('index');
    }
}
