<?php

namespace Smorken\Controller\View\WithService\Concerns;

use Smorken\Service\Contracts\Services\RetrieveService;

trait HasRetrieveService
{
    use HasCrudServices;

    public function getRetrieveService(): RetrieveService
    {
        return $this->getCrudServices()->getRetrieveService();
    }
}
