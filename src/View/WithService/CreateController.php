<?php

namespace Smorken\Controller\View\WithService;

use Smorken\Controller\View\Controller;
use Smorken\Controller\View\WithService\Concerns\HasCreate;
use Smorken\Service\Contracts\Services\CreateService;

abstract class CreateController extends Controller implements \Smorken\Controller\Contracts\View\CreateController
{
    use HasCreate;

    public function __construct(protected CreateService $createService)
    {
        parent::__construct();
    }

    public function getCreateService(): CreateService
    {
        return $this->createService;
    }
}
