<?php

namespace Smorken\Controller\View;

use Illuminate\Support\Facades\View;

abstract class Controller extends \Smorken\Controller\Controller
{
    protected string $baseView = '';

    protected function getViewName(string $name): string
    {
        $parts = [
            trim($this->baseView, '.'),
            trim($name, '.'),
        ];

        return implode('.', array_filter($parts));
    }

    protected function initializeController(): void
    {
        $this->shareControllerClassToView();
        $this->shareTemplateToView();
    }

    protected function makeView(string $name): \Illuminate\Contracts\View\View
    {
        return View::make($name);
    }

    protected function shareControllerClassToView(): void
    {
        View::share('controller', $this->getControllerClass());
    }

    protected function shareTemplateToView(): void
    {
        if (property_exists($this, 'template') && $this->template) {
            View::share('template', $this->template);
        }
    }
}
