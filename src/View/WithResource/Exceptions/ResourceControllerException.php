<?php

declare(strict_types=1);

namespace Smorken\Controller\View\WithResource\Exceptions;

class ResourceControllerException extends \Exception
{
    public static function create(string $message = ''): self
    {
        return new self($message);
    }
}
