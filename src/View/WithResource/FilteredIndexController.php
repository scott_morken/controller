<?php

declare(strict_types=1);

namespace Smorken\Controller\View\WithResource;

use Smorken\Controller\View\Controller;
use Smorken\Controller\View\WithResource\Concerns\HasFilteredIndex;

class FilteredIndexController extends Controller
{
    use HasFilteredIndex;
}
