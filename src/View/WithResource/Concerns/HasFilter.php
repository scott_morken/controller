<?php

declare(strict_types=1);

namespace Smorken\Controller\View\WithResource\Concerns;

use Illuminate\Http\Request;
use Smorken\QueryStringFilter\Contracts\QueryStringFilter;
use Smorken\Support\Contracts\Filter;

/**
 * @method Filter|QueryStringFilter getFilterFromRequest(Request $request)
 */
trait HasFilter
{
    protected function getFilter(Request $request): Filter|QueryStringFilter
    {
        if (method_exists($this, 'getFilterFromRequest')) {
            return $this->getFilterFromRequest($request);
        }

        return \Smorken\QueryStringFilter\QueryStringFilter::from($request)
            ->setKeyValues($request->has('page') ? ['page'] : []);
    }

    protected function getParams(Request $request): array
    {
        return $this->getFilter($request)->toArray();
    }
}
