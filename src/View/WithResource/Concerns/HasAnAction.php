<?php

declare(strict_types=1);

namespace Smorken\Controller\View\WithResource\Concerns;

trait HasAnAction
{
    protected function isADeleteAction(): bool
    {
        return $this->isAnAction(\Smorken\Controller\Contracts\View\WithResource\WithDeleteAction::class);
    }

    protected function isAnAction(string $action): bool
    {
        return is_a($this, $action);
    }

    protected function isAnActionFactory(): bool
    {
        return $this->isAnAction(\Smorken\Controller\Contracts\View\WithResource\WithActionFactory::class);
    }

    protected function isAnUpsertAction(): bool
    {
        return $this->isAnAction(\Smorken\Controller\Contracts\View\WithResource\WithUpsertAction::class);
    }
}
