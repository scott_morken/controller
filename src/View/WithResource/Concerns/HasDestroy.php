<?php

declare(strict_types=1);

namespace Smorken\Controller\View\WithResource\Concerns;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Smorken\Controller\View\WithResource\Exceptions\ResourceControllerException;
use Smorken\Domain\Actions\Contracts\Results\DeleteResult;

trait HasDestroy
{
    use HasAnAction, HasARepository, HasFilter, HasFlashMessages, \Smorken\Controller\View\WithResource\Concerns\WithActionFactory, \Smorken\Controller\View\WithResource\Concerns\WithDeleteAction;

    public function destroy(Request $request, int|string $id): RedirectResponse
    {
        $result = $this->executeDestroyAction($request, $id);

        return $this->onDeleteDoRedirect($request, $result);
    }

    protected function clearCacheOnDestroy(DeleteResult $result): void
    {
        if ($this->isARepositoryFactory()) {
            $this->getRepositoryFactory()->reset($result->id);

            return;
        }
        $this->clearCacheOnDestroyOverride($result);
    }

    protected function clearCacheOnDestroyOverride(DeleteResult $result): void
    {
        //
    }

    protected function deleteRedirect(Request $request, DeleteResult $result): RedirectResponse
    {
        return Response::redirectToAction($this->getDeleteRedirectAction($request, $result),
            $this->getParams($request));
    }

    protected function executeDestroyAction(Request $request, int|string $id): DeleteResult
    {
        if ($this->isADeleteAction()) {
            return ($this->getDeleteAction())($id);
        }
        if ($this->isAnActionFactory()) {
            return $this->getActionFactory()->delete($id);
        }

        return $this->executeDestroyActionOverride($request, $id);
    }

    protected function executeDestroyActionOverride(Request $request, int|string $id): DeleteResult
    {
        throw ResourceControllerException::create('No destroy action method implemented.');
    }

    protected function getDeleteRedirectAction(Request $request, DeleteResult $result): array|string
    {
        return $this->actionArray('index');
    }

    protected function onDeleteDoRedirect(Request $request, DeleteResult $result): RedirectResponse
    {
        $this->addMessagesToRequest($request, $result->messages);
        if ($result->deleted) {
            $this->clearCacheOnDestroy($result);

            return $this->onDeleteSuccessDoRedirect($request, $result);
        }

        return $this->onDeleteFailureDoRedirect($request, $result);
    }

    protected function onDeleteFailureDoRedirect(Request $request, DeleteResult $result): RedirectResponse
    {
        $request->session()->flash('flash:danger', sprintf('Error deleting resource [%s].', $result->id ?? 'unknown'));
        if (method_exists($this, 'onDeleteFailure')) {
            $this->onDeleteFailure($request, $result);
        }

        return $this->deleteRedirect($request, $result);
    }

    protected function onDeleteSuccessDoRedirect(Request $request, DeleteResult $result): RedirectResponse
    {
        $request->session()->flash('flash:success', sprintf('Deleted resource [%s].', $result->id ?? 'unknown'));
        if (method_exists($this, 'onDeleteSuccess')) {
            $this->onDeleteSuccess($request, $result);
        }

        return $this->deleteRedirect($request, $result);
    }
}
