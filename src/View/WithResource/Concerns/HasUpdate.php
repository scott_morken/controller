<?php

declare(strict_types=1);

namespace Smorken\Controller\View\WithResource\Concerns;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

trait HasUpdate
{
    use Upserts;

    public function update(Request $request, int|string $id): RedirectResponse
    {
        $result = $this->executeUpsertAction($request, $id);

        return $this->onUpsertDoRedirect($request, $result);
    }
}
