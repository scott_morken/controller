<?php

declare(strict_types=1);

namespace Smorken\Controller\View\WithResource\Concerns;

use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Smorken\Controller\View\WithResource\Exceptions\ResourceControllerException;
use Smorken\Domain\ViewModels\Contracts\FilteredViewModel;
use Smorken\Domain\ViewModels\Contracts\ViewModel;
use Smorken\QueryStringFilter\Contracts\QueryStringFilter;
use Smorken\Support\Contracts\Filter;

trait HasFilteredIndex
{
    use HasARepository, HasFilter, WithFilteredRepository, WithRepositoryFactory;

    public function index(Request $request): View
    {
        $view = $this->makeView($this->getViewName('index'));

        return $this->applyIndexDataToView($view, $request);
    }

    protected function applyIndexDataToView(View $view, Request $request): View
    {
        return $view->with('viewModel', $this->getIndexViewModel($request));
    }

    protected function getFilteredIndexPerPage(): int
    {
        return 20;
    }

    protected function getIndexViewModel(Request $request): FilteredViewModel
    {
        $filter = $this->getFilter($request);
        if ($this->isAFilteredRepository()) {
            return new \Smorken\Domain\ViewModels\FilteredViewModel($filter,
                ($this->getFilteredRepository()($filter, $this->getFilteredIndexPerPage())));
        }
        if ($this->isARepositoryFactory()) {
            return new \Smorken\Domain\ViewModels\FilteredViewModel($filter,
                $this->getRepositoryFactory()->filtered($filter, $this->getFilteredIndexPerPage()));
        }

        return $this->getIndexViewModelOverride($filter, $request);
    }

    protected function getIndexViewModelOverride(Filter|QueryStringFilter $filter, Request $request): ViewModel
    {
        throw ResourceControllerException::create('No filtered models retrieve method implemented.');
    }
}
