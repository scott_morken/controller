<?php

declare(strict_types=1);

namespace Smorken\Controller\View\WithResource\Concerns;

use Smorken\Domain\Repositories\Contracts\FilteredRepository;

trait WithFilteredRepository
{
    protected FilteredRepository $filteredRepository;

    public function getFilteredRepository(): FilteredRepository
    {
        return $this->filteredRepository;
    }
}
