<?php

declare(strict_types=1);

namespace Smorken\Controller\View\WithResource\Concerns;

use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Smorken\Controller\View\WithResource\Exceptions\ResourceControllerException;
use Smorken\Domain\ViewModels\Contracts\RetrieveViewModel;
use Smorken\Domain\ViewModels\Contracts\ViewModel;

trait HasConfirmDelete
{
    use HasFilter, RetrievesViewModel;

    public function confirmDelete(Request $request, int|string $id): View
    {
        $view = $this->makeView($this->getViewName('delete'));

        return $this->applyConfirmDeleteDataToView($view, $request, $id);
    }

    protected function applyConfirmDeleteDataToView(View $view, Request $request, int|string $id): View
    {
        $filter = $this->getFilter($request);
        $viewModel = $this->getConfirmDeleteViewModel($request, $id);
        $viewModel->setFilter($filter);

        return $view->with('viewModel', $viewModel);
    }

    protected function getConfirmDeleteViewModel(Request $request, int|string $id): RetrieveViewModel
    {
        return $this->getRetrieveViewModel($request, $id) ?? $this->getConfirmDeleteViewModelOverride($request, $id);
    }

    protected function getConfirmDeleteViewModelOverride(Request $request, int|string $id): ViewModel
    {
        throw ResourceControllerException::create('No confirm delete model retrieve method implemented.');
    }
}
