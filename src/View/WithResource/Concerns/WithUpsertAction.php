<?php

declare(strict_types=1);

namespace Smorken\Controller\View\WithResource\Concerns;

use Smorken\Domain\Actions\Contracts\UpsertAction;

trait WithUpsertAction
{
    protected UpsertAction $upsertAction;

    public function getUpsertAction(): UpsertAction
    {
        return $this->upsertAction;
    }
}
