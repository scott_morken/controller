<?php

declare(strict_types=1);

namespace Smorken\Controller\View\WithResource\Concerns;

use Illuminate\Http\Request;
use Smorken\Domain\ViewModels\Contracts\RetrieveViewModel;

trait RetrievesViewModel
{
    use HasARepository, WithRepositoryFactory, WithRetrieveRepository;

    protected function getRetrieveViewModel(Request $request, int|string $id): ?RetrieveViewModel
    {
        if ($this->isARetrieveRepository()) {
            return new \Smorken\Domain\ViewModels\RetrieveViewModel(($this->getRetrieveRepository())($id));
        }
        if ($this->isARepositoryFactory()) {
            return new \Smorken\Domain\ViewModels\RetrieveViewModel($this->getRepositoryFactory()->find($id));
        }

        return null;
    }
}
