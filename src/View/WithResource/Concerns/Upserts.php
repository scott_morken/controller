<?php

declare(strict_types=1);

namespace Smorken\Controller\View\WithResource\Concerns;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Smorken\Controller\View\WithResource\Exceptions\ResourceControllerException;
use Smorken\Domain\Actions\Contracts\Results\SaveResult;
use Smorken\Domain\Actions\Contracts\Upsertable;

trait Upserts
{
    use HasAnAction, HasARepository, HasFilter, HasFlashMessages, WithActionFactory, WithUpsertAction;

    protected function clearCacheOnUpsert(SaveResult $result): void
    {
        if ($this->isARepositoryFactory()) {
            $this->getRepositoryFactory()->reset($result->model?->getKey());

            return;
        }
        $this->clearCacheOnUpsertOverride($result);
    }

    protected function clearCacheOnUpsertOverride(SaveResult $result): void
    {
        //
    }

    protected function executeUpsertAction(Request $request, int|string|null $id): SaveResult
    {
        $upsertable = $this->getUpsertableForUpsert($request, $id);
        if ($this->isAnUpsertAction()) {
            return ($this->getUpsertAction())($upsertable);
        }
        if ($this->isAnActionFactory()) {
            return $this->getActionFactory()->upsert($upsertable);
        }

        return $this->executeUpsertActionOverride($request, $id);
    }

    protected function executeUpsertActionOverride(Request $request, int|string|null $id): SaveResult
    {
        throw ResourceControllerException::create('No upsert action method implemented.');
    }

    protected function getIdFromResult(SaveResult $result): string|int
    {
        $model = $result->model;
        if (is_object($model)) {
            if (method_exists($model, 'getKey')) {
                return $model->getKey();
            }
            if (property_exists($model, 'id')) {
                return $model->id;
            }
        }
        if (is_array($model)) {
            return $model['id'] ?? 'unknown';
        }

        return $result->creating ? 'new' : 'unknown';
    }

    protected function getUpsertRedirectAction(Request $request, SaveResult $result): array|string
    {
        return $this->actionArray('index');
    }

    protected function getUpsertableForUpsert(Request $request, int|string|null $id): Upsertable
    {
        return \Smorken\Domain\Actions\Upsertable::fromRequest($request, $id);
    }

    protected function onUpsertDoRedirect(Request $request, SaveResult $result): RedirectResponse
    {
        $this->addMessagesToRequest($request, $result->messages);
        if ($result->saved) {
            $this->clearCacheOnUpsert($result);

            return $this->onUpsertSuccessDoRedirect($request, $result);
        }

        return $this->onUpsertFailureDoRedirect($request, $result);
    }

    protected function onUpsertFailureDoRedirect(Request $request, SaveResult $result): RedirectResponse
    {
        $request->session()
            ->flash('flash:danger', sprintf('Error saving resource [%s].', $this->getIdFromResult($result)));
        if (method_exists($this, 'onUpsertFailure')) {
            $this->onUpsertFailure($request, $result);
        }

        return $this->upsertRedirect($request, $result);
    }

    protected function onUpsertSuccessDoRedirect(Request $request, SaveResult $result): RedirectResponse
    {
        $request->session()
            ->flash('flash:success', sprintf('Saved resource [%s].', $this->getIdFromResult($result)));
        if (method_exists($this, 'onUpsertSuccess')) {
            $this->onUpsertSuccess($request, $result);
        }

        return $this->upsertRedirect($request, $result);
    }

    protected function upsertRedirect(Request $request, SaveResult $result): RedirectResponse
    {
        return Response::redirectToAction($this->getUpsertRedirectAction($request, $result),
            $this->getParams($request));
    }
}
