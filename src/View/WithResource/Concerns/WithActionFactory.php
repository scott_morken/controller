<?php

declare(strict_types=1);

namespace Smorken\Controller\View\WithResource\Concerns;

use Smorken\Domain\Factories\ActionFactory;

trait WithActionFactory
{
    protected ActionFactory $actionFactory;

    public function getActionFactory(): ActionFactory
    {
        return $this->actionFactory;
    }
}
