<?php

declare(strict_types=1);

namespace Smorken\Controller\View\WithResource\Concerns;

trait HasARepository
{
    protected function isAFilteredRepository(): bool
    {
        return $this->isARepository(\Smorken\Controller\Contracts\View\WithResource\WithFilteredRepository::class);
    }

    protected function isARepository(string $contract): bool
    {
        return is_a($this, $contract);
    }

    protected function isARepositoryFactory(): bool
    {
        return $this->isARepository(\Smorken\Controller\Contracts\View\WithResource\WithRepositoryFactory::class);
    }

    protected function isARetrieveRepository(): bool
    {
        return $this->isARepository(\Smorken\Controller\Contracts\View\WithResource\WithRetrieveRepository::class);
    }

    protected function isAnIterableRepository(): bool
    {
        return $this->isARepository(\Smorken\Controller\Contracts\View\WithResource\WithIterableRepository::class);
    }
}
