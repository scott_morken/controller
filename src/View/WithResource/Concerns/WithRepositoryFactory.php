<?php

declare(strict_types=1);

namespace Smorken\Controller\View\WithResource\Concerns;

use Smorken\Domain\Factories\RepositoryFactory;

trait WithRepositoryFactory
{
    protected RepositoryFactory $repositoryFactory;

    public function getRepositoryFactory(): RepositoryFactory
    {
        return $this->repositoryFactory;
    }
}
