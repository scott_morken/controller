<?php

declare(strict_types=1);

namespace Smorken\Controller\View\WithResource\Concerns;

use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Smorken\Controller\View\WithResource\Exceptions\ResourceControllerException;
use Smorken\Domain\ViewModels\Contracts\RetrieveViewModel;
use Smorken\Domain\ViewModels\Contracts\ViewModel;

trait HasShow
{
    use HasFilter, RetrievesViewModel;

    public function show(Request $request, int|string $id): View
    {
        $view = $this->makeView($this->getViewName('show'));

        return $this->applyShowDataToView($view, $request, $id);
    }

    protected function applyShowDataToView(View $view, Request $request, int|string $id): View
    {
        $filter = $this->getFilter($request);
        $viewModel = $this->getShowViewModel($request, $id);
        $viewModel->setFilter($filter);

        return $view->with('viewModel', $viewModel);
    }

    protected function getShowViewModel(Request $request, int|string $id): RetrieveViewModel
    {
        return $this->getRetrieveViewModel($request, $id) ?? $this->getShowViewModelOverride($request, $id);
    }

    protected function getShowViewModelOverride(Request $request, int|string $id): ViewModel
    {
        throw ResourceControllerException::create('No show model retrieve method implemented.');
    }
}
