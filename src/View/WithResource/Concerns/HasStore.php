<?php

declare(strict_types=1);

namespace Smorken\Controller\View\WithResource\Concerns;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

trait HasStore
{
    use Upserts;

    public function store(Request $request): RedirectResponse
    {
        $result = $this->executeUpsertAction($request, null);

        return $this->onUpsertDoRedirect($request, $result);
    }
}
