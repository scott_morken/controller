<?php

declare(strict_types=1);

namespace Smorken\Controller\View\WithResource\Concerns;

use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Smorken\Controller\View\WithResource\Exceptions\ResourceControllerException;
use Smorken\Domain\ViewModels\Contracts\RetrieveViewModel;
use Smorken\Domain\ViewModels\Contracts\ViewModel;

trait HasCreate
{
    use HasARepository, HasFilter, WithRepositoryFactory, WithRetrieveRepository;

    public function create(Request $request): View
    {
        $view = $this->makeView($this->getViewName('create'));

        return $this->applyCreateDataToView($view, $request);
    }

    protected function applyCreateDataToView(View $view, Request $request): View
    {
        $filter = $this->getFilter($request);
        $viewModel = $this->getCreateViewModel($request);
        $viewModel->setFilter($filter);

        return $view->with('viewModel', $viewModel);
    }

    protected function getCreateViewModel(Request $request): RetrieveViewModel
    {
        if ($this->isARetrieveRepository()) {
            return new \Smorken\Domain\ViewModels\RetrieveViewModel($this->getRetrieveRepository()->emptyModel());
        }
        if ($this->isARepositoryFactory()) {
            return new \Smorken\Domain\ViewModels\RetrieveViewModel($this->getRepositoryFactory()->emptyModel());
        }

        return $this->getCreateViewModelOverride($request);
    }

    protected function getCreateViewModelOverride(Request $request): ViewModel
    {
        throw ResourceControllerException::create('No create model retrieve method implemented.');
    }
}
