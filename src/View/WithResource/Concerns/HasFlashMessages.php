<?php

declare(strict_types=1);

namespace Smorken\Controller\View\WithResource\Concerns;

use Illuminate\Contracts\Support\MessageBag;
use Illuminate\Http\Request;

trait HasFlashMessages
{
    protected function addMessagesToRequest(Request $request, ?MessageBag $messages): void
    {
        if (! $messages) {
            return;
        }
        $counter = 0;
        foreach ($messages->toArray() as $key => $keyMessages) {
            foreach ($keyMessages as $message) {
                if ($request->session()->has($key)) {
                    $key = $key.':'.$counter;
                    $counter++;
                }
                $request->session()->flash($key, $message);
            }
        }
    }
}
