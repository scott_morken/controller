<?php

declare(strict_types=1);

namespace Smorken\Controller\View\WithResource\Concerns;

use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Smorken\Controller\View\WithResource\Exceptions\ResourceControllerException;
use Smorken\Domain\ViewModels\Contracts\RetrieveViewModel;
use Smorken\Domain\ViewModels\Contracts\ViewModel;

trait HasEdit
{
    use HasFilter, RetrievesViewModel;

    public function edit(Request $request, int|string $id): View
    {
        $view = $this->makeView($this->getViewName('edit'));

        return $this->applyEditDataToView($view, $request, $id);
    }

    protected function applyEditDataToView(View $view, Request $request, int|string $id): View
    {
        $filter = $this->getFilter($request);
        $viewModel = $this->getEditViewModel($request, $id);
        $viewModel->setFilter($filter);

        return $view->with('viewModel', $viewModel);
    }

    protected function getEditViewModel(Request $request, int|string $id): RetrieveViewModel
    {
        return $this->getRetrieveViewModel($request, $id) ?? $this->getEditViewModelOverride($request, $id);
    }

    protected function getEditViewModelOverride(Request $request, int|string $id): ViewModel
    {
        throw ResourceControllerException::create('No edit model retrieve method implemented.');
    }
}
