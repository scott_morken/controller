<?php

declare(strict_types=1);

namespace Smorken\Controller\View\WithResource\Concerns;

use Smorken\Domain\Repositories\Contracts\RetrieveRepository;

trait WithRetrieveRepository
{
    protected RetrieveRepository $retrieveRepository;

    public function getRetrieveRepository(): RetrieveRepository
    {
        return $this->retrieveRepository;
    }
}
