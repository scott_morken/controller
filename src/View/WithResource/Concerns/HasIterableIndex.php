<?php

declare(strict_types=1);

namespace Smorken\Controller\View\WithResource\Concerns;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Smorken\Controller\View\WithResource\Exceptions\ResourceControllerException;
use Smorken\Domain\ViewModels\Contracts\IterableViewModel;
use Smorken\Domain\ViewModels\Contracts\ViewModel;

trait HasIterableIndex
{
    use HasARepository, WithIterableRepository, WithRepositoryFactory;

    public function index(Request $request): View
    {
        $view = $this->makeView($this->getViewName('index'));

        return $this->applyIndexDataToView($view, $request);
    }

    protected function applyIndexDataToView(View $view, Request $request): View
    {
        return $view->with('viewModel', $this->getIndexViewModel($request));
    }

    protected function getIndexViewModel(Request $request): IterableViewModel
    {
        if ($this->isAnIterableRepository()) {
            return new \Smorken\Domain\ViewModels\IterableViewModel(($this->getIterableRepository())($this->getIterableIndexPerPage()));
        }
        if ($this->isARepositoryFactory()) {
            $method = $this->getRepositoryFactoryMethod();

            return new \Smorken\Domain\ViewModels\IterableViewModel($this->getRepositoryFactoryModels($method));
        }

        return $this->getIndexViewModelOverride($request);
    }

    protected function getIndexViewModelOverride(Request $request): ViewModel
    {
        throw ResourceControllerException::create('No iterable models retrieve method implemented.');
    }

    protected function getIterableIndexPerPage(): int
    {
        return 20;
    }

    protected function getRepositoryFactoryMethod(): string
    {
        return 'get';
    }

    protected function getRepositoryFactoryModels(string $method): Collection|Paginator
    {
        return match ($method) {
            'all' => $this->getRepositoryFactory()->all(),
            default => $this->getRepositoryFactory()->get($this->getIterableIndexPerPage()),
        };
    }
}
