<?php

declare(strict_types=1);

namespace Smorken\Controller\View\WithResource\Concerns;

use Smorken\Domain\Repositories\Contracts\IterableRepository;

trait WithIterableRepository
{
    protected IterableRepository $iterableRepository;

    public function getIterableRepository(): IterableRepository
    {
        return $this->iterableRepository;
    }
}
