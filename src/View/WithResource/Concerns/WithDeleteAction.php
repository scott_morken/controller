<?php

declare(strict_types=1);

namespace Smorken\Controller\View\WithResource\Concerns;

use Smorken\Domain\Actions\Contracts\DeleteAction;

trait WithDeleteAction
{
    protected DeleteAction $deleteAction;

    public function getDeleteAction(): DeleteAction
    {
        return $this->deleteAction;
    }
}
