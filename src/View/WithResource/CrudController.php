<?php

declare(strict_types=1);

namespace Smorken\Controller\View\WithResource;

use Smorken\Controller\View\Controller;
use Smorken\Controller\View\WithResource\Concerns\HasConfirmDelete;
use Smorken\Controller\View\WithResource\Concerns\HasCreate;
use Smorken\Controller\View\WithResource\Concerns\HasDestroy;
use Smorken\Controller\View\WithResource\Concerns\HasEdit;
use Smorken\Controller\View\WithResource\Concerns\HasFilteredIndex;
use Smorken\Controller\View\WithResource\Concerns\HasShow;
use Smorken\Controller\View\WithResource\Concerns\HasStore;
use Smorken\Controller\View\WithResource\Concerns\HasUpdate;

class CrudController extends Controller
{
    use HasConfirmDelete, HasCreate, HasDestroy, HasEdit, HasFilteredIndex, HasShow, HasStore, HasUpdate;
}
