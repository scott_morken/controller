<?php

declare(strict_types=1);

namespace Smorken\Controller\View\WithResource;

use Smorken\Controller\View\Controller;
use Smorken\Controller\View\WithResource\Concerns\HasCreate;

class CreateController extends Controller
{
    use HasCreate;
}
