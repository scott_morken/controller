<?php

namespace Smorken\Controller;

abstract class Controller
{
    public function __construct()
    {
        $this->initializeController();
    }

    protected function actionArray(?string $method): string|array
    {
        if ($method === null) {
            return $this->getControllerClass();
        }

        return [$this->getControllerClass(), $method];
    }

    protected function getControllerClass(): string
    {
        return static::class;
    }

    protected function initializeController(): void
    {
    }
}
