<?php

namespace Smorken\Controller\Contracts\View;

use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Smorken\Controller\Contracts\View\WithService\HasIndexService;

interface IndexController extends HasIndexService
{
    public function index(Request $request): View;
}
