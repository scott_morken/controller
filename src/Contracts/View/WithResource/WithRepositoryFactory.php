<?php

declare(strict_types=1);

namespace Smorken\Controller\Contracts\View\WithResource;

use Smorken\Domain\Factories\RepositoryFactory;

interface WithRepositoryFactory
{
    public function getRepositoryFactory(): RepositoryFactory;
}
