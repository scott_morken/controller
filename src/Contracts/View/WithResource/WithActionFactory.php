<?php

declare(strict_types=1);

namespace Smorken\Controller\Contracts\View\WithResource;

use Smorken\Domain\Factories\ActionFactory;

interface WithActionFactory
{
    public function getActionFactory(): ActionFactory;
}
