<?php

declare(strict_types=1);

namespace Smorken\Controller\Contracts\View\WithResource;

use Smorken\Domain\Actions\Contracts\DeleteAction;

interface WithDeleteAction
{
    public function getDeleteAction(): DeleteAction;
}
