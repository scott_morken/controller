<?php

declare(strict_types=1);

namespace Smorken\Controller\Contracts\View\WithResource;

use Smorken\Domain\Repositories\Contracts\RetrieveRepository;

interface WithRetrieveRepository
{
    public function getRetrieveRepository(): RetrieveRepository;
}
