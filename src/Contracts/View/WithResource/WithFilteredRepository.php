<?php

declare(strict_types=1);

namespace Smorken\Controller\Contracts\View\WithResource;

use Smorken\Domain\Repositories\Contracts\FilteredRepository;

interface WithFilteredRepository
{
    public function getFilteredRepository(): FilteredRepository;
}
