<?php

declare(strict_types=1);

namespace Smorken\Controller\Contracts\View\WithResource;

use Smorken\Domain\Repositories\Contracts\IterableRepository;

interface WithIterableRepository
{
    public function getIterableRepository(): IterableRepository;
}
