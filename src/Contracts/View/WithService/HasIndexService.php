<?php

namespace Smorken\Controller\Contracts\View\WithService;

use Smorken\Service\Contracts\Services\IndexService;

interface HasIndexService
{
    public function getIndexService(): IndexService;
}
