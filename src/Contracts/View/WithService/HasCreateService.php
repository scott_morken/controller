<?php

namespace Smorken\Controller\Contracts\View\WithService;

use Smorken\Service\Contracts\Services\CreateService;

interface HasCreateService
{
    public function getCreateService(): CreateService;
}
