<?php

namespace Smorken\Controller\Contracts\View\WithService;

use Smorken\Service\Contracts\Services\UpdateService;

interface HasUpdateService
{
    public function getUpdateService(): UpdateService;
}
