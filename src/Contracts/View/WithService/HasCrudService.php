<?php

namespace Smorken\Controller\Contracts\View\WithService;

use Smorken\Service\Contracts\Services\CrudServices;

interface HasCrudService
{
    public function getCrudService(): CrudServices;
}
