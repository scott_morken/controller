<?php

namespace Smorken\Controller\Contracts\View\WithService;

use Smorken\Service\Contracts\Services\RetrieveService;

interface HasRetrieveService
{
    public function getRetrieveService(): RetrieveService;
}
