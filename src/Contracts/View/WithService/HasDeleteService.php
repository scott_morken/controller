<?php

namespace Smorken\Controller\Contracts\View\WithService;

use Smorken\Service\Contracts\Services\DeleteService;

interface HasDeleteService
{
    public function getDeleteService(): DeleteService;
}
