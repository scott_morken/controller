<?php

namespace Smorken\Controller\Contracts\View;

use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

interface DeleteController
{
    public function delete(Request $request, int|string $id): View;

    public function doDelete(Request $request, int|string $id): RedirectResponse;
}
