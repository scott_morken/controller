<?php

namespace Smorken\Controller\Contracts\View;

use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

interface UpdateController
{
    public function doUpdate(Request $request, int|string $id): RedirectResponse;

    public function update(Request $request, int|string $id): View;
}
