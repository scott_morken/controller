<?php

namespace Smorken\Controller\Contracts\View;

use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

interface ViewController
{
    public function view(Request $request, int|string $id): View;
}
