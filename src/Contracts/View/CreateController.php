<?php

namespace Smorken\Controller\Contracts\View;

use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

interface CreateController
{
    public function create(Request $request): View;

    public function doCreate(Request $request): RedirectResponse;
}
