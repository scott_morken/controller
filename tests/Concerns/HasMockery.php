<?php

namespace Tests\Smorken\Controller\Concerns;

use Mockery as m;

trait HasMockery
{
    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
