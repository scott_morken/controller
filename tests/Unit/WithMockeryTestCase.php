<?php

declare(strict_types=1);

namespace Tests\Smorken\Controller\Unit;

use Illuminate\Support\Facades\View;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class WithMockeryTestCase extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        View::clearResolvedInstances();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
