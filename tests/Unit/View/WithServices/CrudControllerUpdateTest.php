<?php

namespace Tests\Smorken\Controller\Unit\View\WithServices;

use Illuminate\Contracts\Session\Session;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use Illuminate\Translation\ArrayLoader;
use Illuminate\Translation\Translator;
use Illuminate\Validation\Factory;
use Illuminate\Validation\ValidationException;
use Mockery as m;
use Smorken\Model\VO;
use Smorken\Service\Contracts\Services\ValidatorService;
use Smorken\Service\Services\AllByStorageProviderService;
use Smorken\Service\Services\CrudByStorageProviderServices;
use Smorken\Storage\Contracts\Base;
use Smorken\Support\Contracts\Filter;
use Tests\Smorken\Controller\Stubs\View\WithServices\CrudControllerStub;
use Tests\Smorken\Controller\Unit\WithMockeryTestCase;

class CrudControllerUpdateTest extends WithMockeryTestCase
{
    public function testDoUpdate(): void
    {
        View::shouldReceive('share')->once()->with('controller', CrudControllerStub::class);
        $provider = m::mock(Base::class);
        $model = new VO();
        $provider->shouldReceive('getModel')->andReturn($model);
        $indexService = new AllByStorageProviderService($provider);
        $crudService = CrudByStorageProviderServices::createByStorageProvider($provider);
        $sut = new CrudControllerStub($crudService, $indexService);
        $updatable = new VO(['id' => 1, 'foo' => 'bar']);
        $provider->shouldReceive('findOrFail')->once()->with(1)->andReturn($updatable);
        $updated = new VO(['id' => 1, 'foo' => 'biz']);
        $provider->shouldReceive('update')->once()
            ->with($updatable, ['foo' => 'biz'])
            ->andReturn($updated);
        $request = new Request(['foo' => 'biz']);
        $request->setLaravelSession(m::mock(Session::class));
        $request->session()->shouldReceive('flash')
            ->once()
            ->with('flash:success', 'Saved resource [1].');
        $mockResponse = m::mock(RedirectResponse::class);
        Redirect::shouldReceive('action')
            ->once()
            ->with([CrudControllerStub::class, 'index'], ['foo' => 'biz'])
            ->andReturn($mockResponse);
        $response = $sut->doUpdate($request, 1);
        $this->assertSame($mockResponse, $response);
    }

    public function testDoUpdateFails(): void
    {
        View::shouldReceive('share')->once()->with('controller', CrudControllerStub::class);
        $provider = m::mock(Base::class);
        $model = new VO();
        $provider->shouldReceive('getModel')->andReturn($model);
        $indexService = new AllByStorageProviderService($provider);
        $crudService = CrudByStorageProviderServices::createByStorageProvider($provider);
        $sut = new CrudControllerStub($crudService, $indexService);
        $updatable = new VO(['id' => 1, 'foo' => 'bar']);
        $provider->shouldReceive('findOrFail')->once()->with(1)->andReturn($updatable);
        $provider->shouldReceive('update')->once()
            ->with($updatable, ['foo' => 'biz'])
            ->andReturn(null);
        $request = new Request(['foo' => 'biz']);
        $request->setLaravelSession(m::mock(Session::class));
        $request->session()->shouldReceive('flash')
            ->once()
            ->with('flash:danger', 'Error saving resource [1].');
        $mockResponse = m::mock(RedirectResponse::class);
        Redirect::shouldReceive('back')
            ->once()
            ->andReturn($mockResponse);
        $mockResponse->shouldReceive('withInput')
            ->once()
            ->andReturnSelf();
        $response = $sut->doUpdate($request, 1);
        $this->assertSame($mockResponse, $response);
    }

    public function testDoUpdateFailsValidation(): void
    {
        View::shouldReceive('share')->once()->with('controller', CrudControllerStub::class);
        $provider = m::mock(Base::class);
        $indexService = new AllByStorageProviderService($provider);
        $crudService = CrudByStorageProviderServices::createByStorageProvider($provider, [
            ValidatorService::class => $this->getValidatorService(),
        ]);
        $sut = new CrudControllerStub($crudService, $indexService);
        $provider->shouldReceive('validationRules')->andReturn(['foo' => 'required']);
        $provider->shouldReceive('update')->never();
        $request = new Request([]);
        $this->expectException(ValidationException::class);
        $response = $sut->doUpdate($request, 1);
    }

    public function testUpdate(): void
    {
        View::shouldReceive('share')->once()->with('controller', CrudControllerStub::class);
        $mockView = m::mock(\Illuminate\Contracts\View\View::class);
        $provider = m::mock(Base::class);
        $indexService = new AllByStorageProviderService($provider);
        $crudService = CrudByStorageProviderServices::createByStorageProvider($provider);
        $sut = new CrudControllerStub($crudService, $indexService);
        $provider->shouldReceive('getModel')->andReturn(new VO());
        $model = new VO(['id' => 1, 'foo' => 'bar']);
        $provider->shouldReceive('findOrFail')->once()->with(1)->andReturn($model);
        View::shouldReceive('make')->once()->with('update')->andReturn($mockView);
        $mockView->shouldReceive('with')->once()->with('filter', m::type(Filter::class))->andReturnSelf();
        $mockView->shouldReceive('with')->once()->with('model', $model)->andReturnSelf();
        $v = $sut->update(new Request(), 1);
        $this->assertSame($mockView, $v);
    }

    protected function getValidatorService(): ValidatorService
    {
        return new \Smorken\Service\Services\ValidatorService(new Factory(new Translator(new ArrayLoader(), 'en')));
    }
}
