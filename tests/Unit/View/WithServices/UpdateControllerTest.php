<?php

namespace Tests\Smorken\Controller\Unit\View\WithServices;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Mockery as m;
use Smorken\Model\VO;
use Smorken\Service\Services\RetrieveByStorageProviderService;
use Smorken\Service\Services\UpdateByStorageProviderService;
use Smorken\Storage\Contracts\Base;
use Smorken\Support\Contracts\Filter;
use Tests\Smorken\Controller\Stubs\View\WithServices\UpdateControllerStub;
use Tests\Smorken\Controller\Unit\WithMockeryTestCase;

class UpdateControllerTest extends WithMockeryTestCase
{
    public function testUpdate(): void
    {
        View::shouldReceive('share')->once()->with('controller', UpdateControllerStub::class);
        $mockView = m::mock(\Illuminate\Contracts\View\View::class);
        $provider = m::mock(Base::class);
        $updateService = new UpdateByStorageProviderService($provider);
        $retrieveService = new RetrieveByStorageProviderService($provider);
        $sut = new UpdateControllerStub($updateService, $retrieveService);
        $provider->shouldReceive('getModel')->andReturn(new VO());
        $model = new VO(['id' => 1, 'foo' => 'bar']);
        $provider->shouldReceive('findOrFail')->once()->with(1)->andReturn($model);
        View::shouldReceive('make')->once()->with('update')->andReturn($mockView);
        $mockView->shouldReceive('with')->once()->with('filter', m::type(Filter::class))->andReturnSelf();
        $mockView->shouldReceive('with')->once()->with('model', $model)->andReturnSelf();
        $v = $sut->update(new Request(), 1);
        $this->assertSame($mockView, $v);
    }
}
