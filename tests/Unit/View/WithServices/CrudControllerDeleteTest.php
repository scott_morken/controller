<?php

namespace Tests\Smorken\Controller\Unit\View\WithServices;

use Illuminate\Contracts\Session\Session;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use Mockery as m;
use Smorken\Model\VO;
use Smorken\Service\Services\AllByStorageProviderService;
use Smorken\Service\Services\CrudByStorageProviderServices;
use Smorken\Storage\Contracts\Base;
use Smorken\Support\Contracts\Filter;
use Tests\Smorken\Controller\Stubs\View\WithServices\CrudControllerStub;
use Tests\Smorken\Controller\Unit\WithMockeryTestCase;

class CrudControllerDeleteTest extends WithMockeryTestCase
{
    public function testDelete(): void
    {
        View::shouldReceive('share')->once()->with('controller', CrudControllerStub::class);
        $mockView = m::mock(\Illuminate\Contracts\View\View::class);
        $provider = m::mock(Base::class);
        $indexService = new AllByStorageProviderService($provider);
        $crudService = CrudByStorageProviderServices::createByStorageProvider($provider);
        $sut = new CrudControllerStub($crudService, $indexService);
        $provider->shouldReceive('getModel')->andReturn(new VO());
        $model = new VO(['id' => 1, 'foo' => 'bar']);
        $provider->shouldReceive('findOrFail')->once()->with(1)->andReturn($model);
        View::shouldReceive('make')->once()->with('delete')->andReturn($mockView);
        $mockView->shouldReceive('with')->once()->with('filter', m::type(Filter::class))->andReturnSelf();
        $mockView->shouldReceive('with')->once()->with('model', $model)->andReturnSelf();
        $v = $sut->delete(new Request(), 1);
        $this->assertSame($mockView, $v);
    }

    public function testDoDelete(): void
    {
        View::shouldReceive('share')->once()->with('controller', CrudControllerStub::class);
        $provider = m::mock(Base::class);
        $model = new VO();
        $provider->shouldReceive('getModel')->andReturn($model);
        $indexService = new AllByStorageProviderService($provider);
        $crudService = CrudByStorageProviderServices::createByStorageProvider($provider);
        $sut = new CrudControllerStub($crudService, $indexService);
        $deletable = new VO(['id' => 1, 'foo' => 'bar']);
        $provider->shouldReceive('findOrFail')->once()->with(1)->andReturn($deletable);
        $provider->shouldReceive('delete')->once()
            ->with($deletable)
            ->andReturn(true);
        $request = new Request([]);
        $request->setLaravelSession(m::mock(Session::class));
        $request->session()->shouldReceive('flash')
            ->once()
            ->with('flash:success', 'Deleted resource [1].');
        $mockResponse = m::mock(RedirectResponse::class);
        Redirect::shouldReceive('action')
            ->once()
            ->with([CrudControllerStub::class, 'index'], [])
            ->andReturn($mockResponse);
        $response = $sut->doDelete($request, 1);
        $this->assertSame($mockResponse, $response);
    }

    public function testDoDeleteFails(): void
    {
        View::shouldReceive('share')->once()->with('controller', CrudControllerStub::class);
        $provider = m::mock(Base::class);
        $model = new VO();
        $provider->shouldReceive('getModel')->andReturn($model);
        $indexService = new AllByStorageProviderService($provider);
        $crudService = CrudByStorageProviderServices::createByStorageProvider($provider);
        $sut = new CrudControllerStub($crudService, $indexService);
        $deletable = new VO(['id' => 1, 'foo' => 'bar']);
        $provider->shouldReceive('findOrFail')->once()->with(1)->andReturn($deletable);
        $provider->shouldReceive('delete')->once()
            ->with($deletable)
            ->andReturn(false);
        $request = new Request([]);
        $request->setLaravelSession(m::mock(Session::class));
        $request->session()->shouldReceive('flash')
            ->once()
            ->with('flash:danger', 'Error deleting resource [1].');
        $mockResponse = m::mock(RedirectResponse::class);
        Redirect::shouldReceive('action')
            ->once()
            ->with([CrudControllerStub::class, 'index'], [])
            ->andReturn($mockResponse);
        $response = $sut->doDelete($request, 1);
        $this->assertSame($mockResponse, $response);
    }
}
