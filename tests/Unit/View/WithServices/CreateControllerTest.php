<?php

namespace Tests\Smorken\Controller\Unit\View\WithServices;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Mockery as m;
use Smorken\Model\VO;
use Smorken\Service\Services\CreateByStorageProviderService;
use Smorken\Storage\Contracts\Base;
use Smorken\Support\Contracts\Filter;
use Tests\Smorken\Controller\Stubs\View\WithServices\CreateControllerStub;
use Tests\Smorken\Controller\Unit\WithMockeryTestCase;

class CreateControllerTest extends WithMockeryTestCase
{
    public function testCreate(): void
    {
        View::shouldReceive('share')->once()->with('controller', CreateControllerStub::class);
        $mockView = m::mock(\Illuminate\Contracts\View\View::class);
        $provider = m::mock(Base::class);
        $service = new CreateByStorageProviderService($provider);
        $sut = new CreateControllerStub($service);
        $model = new VO();
        $provider->shouldReceive('getModel')->once()->andReturn($model);
        View::shouldReceive('make')->once()->with('create')->andReturn($mockView);
        $mockView->shouldReceive('with')->once()->with('filter', m::type(Filter::class))->andReturnSelf();
        $mockView->shouldReceive('with')->once()->with('model', $model)->andReturnSelf();
        $v = $sut->create(new Request());
        $this->assertSame($mockView, $v);
    }
}
