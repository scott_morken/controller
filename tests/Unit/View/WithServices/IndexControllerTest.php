<?php

namespace Tests\Smorken\Controller\Unit\View\WithServices;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\View;
use Mockery as m;
use Smorken\Controller\View\Controller;
use Smorken\Service\Services\AllByStorageProviderService;
use Smorken\Service\Services\FilterService;
use Smorken\Service\Services\PaginateByFilterService;
use Smorken\Storage\Contracts\Base;
use Smorken\Support\Contracts\Filter;
use Tests\Smorken\Controller\Stubs\View\TestModelStub;
use Tests\Smorken\Controller\Stubs\View\WithServices\IndexAllControllerStub;
use Tests\Smorken\Controller\Stubs\View\WithServices\IndexControllerStub;
use Tests\Smorken\Controller\Stubs\View\WithServices\IndexPaginatedControllerStub;
use Tests\Smorken\Controller\Unit\WithMockeryTestCase;

class IndexControllerTest extends WithMockeryTestCase
{
    public function testDefaultIndexController(): void
    {
        View::shouldReceive('share')->once()->with('controller', IndexControllerStub::class);
        $mockView = m::mock(\Illuminate\Contracts\View\View::class);
        $service = new AllByStorageProviderService(m::mock(Base::class));
        $sut = new IndexControllerStub($service);
        View::shouldReceive('make')->once()->with('index')->andReturn($mockView);
        $all = new Collection();
        $service->getProvider()->shouldReceive('all')->once()->andReturn($all);
        $mockView->shouldReceive('with')->once()->with('models', $all)->andReturnSelf();
        $mockView->shouldReceive('with')->once()->with('filter', null)->andReturnSelf();
        $v = $sut->index(new Request());
        $this->assertSame($mockView, $v);
    }

    public function testIndexWithAllByStorageProviderService(): void
    {
        View::shouldReceive('share')->once()->with('controller', IndexAllControllerStub::class);
        $mockView = m::mock(\Illuminate\Contracts\View\View::class);
        $service = new AllByStorageProviderService(m::mock(Base::class));
        $sut = new IndexAllControllerStub($service);
        View::shouldReceive('make')->once()->with('foo.index')->andReturn($mockView);
        $all = new Collection();
        $service->getProvider()->shouldReceive('all')->once()->andReturn($all);
        $mockView->shouldReceive('with')->once()->with('models', $all)->andReturnSelf();
        $mockView->shouldReceive('with')->once()->with('filter', null)->andReturnSelf();
        $v = $sut->index(new Request());
        $this->assertSame($mockView, $v);
    }

    public function testIndexWithPaginateByFilterService(): void
    {
        View::shouldReceive('share')->once()->with('controller', IndexPaginatedControllerStub::class);
        $mockView = m::mock(\Illuminate\Contracts\View\View::class);
        $service = new PaginateByFilterService(m::mock(Base::class),
            [\Smorken\Service\Contracts\Services\FilterService::class => new FilterService()]);
        $sut = new IndexPaginatedControllerStub($service);
        View::shouldReceive('make')->once()->with('foo.index')->andReturn($mockView);
        $paginated = new LengthAwarePaginator([], 0, 20);
        $service->getProvider()->shouldReceive('getByFilter')->once()->with(m::type(Filter::class), 20)
            ->andReturn($paginated);
        $service->getProvider()->shouldReceive('getModel')->once()->andReturn(new TestModelStub());
        $mockView->shouldReceive('with')->once()->with('models', $paginated)->andReturnSelf();
        $mockView->shouldReceive('with')->once()->with('filter', m::type(Filter::class))->andReturnSelf();
        $v = $sut->index(new Request());
        $this->assertSame($mockView, $v);
    }

    public function testInstantiationSharesControllerName(): void
    {
        View::shouldReceive('share')->once()->with('controller', IndexAllControllerStub::class);
        $service = new AllByStorageProviderService(m::mock(Base::class));
        $sut = new IndexAllControllerStub($service);
        $this->assertInstanceOf(Controller::class, $sut);
    }
}
