<?php

declare(strict_types=1);

namespace Tests\Smorken\Controller\Unit\View\WithResource;

use Illuminate\Contracts\Session\Session;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\View;
use Illuminate\Support\MessageBag;
use Mockery as m;
use Smorken\Controller\Contracts\View\WithResource\WithRetrieveRepository;
use Smorken\Controller\Contracts\View\WithResource\WithUpsertAction;
use Smorken\Controller\View\WithResource\CrudController;
use Smorken\Domain\Actions\Contracts\Upsertable;
use Smorken\Domain\Actions\Contracts\UpsertAction;
use Smorken\Domain\Actions\Results\SaveResult;
use Smorken\Domain\Repositories\Contracts\RetrieveRepository;
use Smorken\Domain\ViewModels\Contracts\RetrieveViewModel;
use Tests\Smorken\Controller\Unit\WithMockeryTestCase;

class CrudControllerUpdateRecordTest extends WithMockeryTestCase
{
    public function test_it_can_update_a_record_using_upsert_action(): void
    {
        View::shouldReceive('share')->once()->with('controller', m::type('string'));
        $upsertAction = m::mock(UpsertAction::class);
        $model = m::mock(\Smorken\Model\Contracts\Model::class);
        $model->shouldReceive('getKey')->andReturn(1);
        $upsertAction->shouldReceive('__invoke')
            ->once()
            ->with(m::type(Upsertable::class))
            ->andReturn(new SaveResult($model, true, true, new MessageBag()));
        $request = new Request([]);
        $request->setLaravelSession(m::mock(Session::class));
        $request->session()->shouldReceive('flash')
            ->once()
            ->with('flash:success', 'Saved resource [1].');
        $mockResponse = m::mock(RedirectResponse::class);
        Response::shouldReceive('redirectToAction')
            ->once()
            ->with(m::type('array'), [])
            ->andReturn($mockResponse);
        $sut = new class($upsertAction) extends CrudController implements WithUpsertAction
        {
            use \Smorken\Controller\View\WithResource\Concerns\WithUpsertAction;

            public function __construct(UpsertAction $upsertAction)
            {
                parent::__construct();
                $this->upsertAction = $upsertAction;
            }
        };
        $this->assertSame($mockResponse, $sut->update($request, 1));
    }

    public function test_it_can_use_a_retrieve_repository(): void
    {
        View::shouldReceive('share')->once()->with('controller', m::type('string'));
        $mockView = m::mock(\Illuminate\Contracts\View\View::class);
        View::shouldReceive('make')->once()->with('edit')->andReturn($mockView);
        $mockRetrieveRepository = m::mock(RetrieveRepository::class);
        $model = m::mock(Model::class);
        $mockRetrieveRepository->shouldReceive('__invoke')
            ->once()
            ->with(1)
            ->andReturn($model);
        $mockView->shouldReceive('with')
            ->once()
            ->with('viewModel', m::type(RetrieveViewModel::class))
            ->andReturnSelf();
        $sut = new class($mockRetrieveRepository) extends CrudController implements WithRetrieveRepository
        {
            public function __construct(RetrieveRepository $retrieveRepository)
            {
                $this->retrieveRepository = $retrieveRepository;
                parent::__construct();
            }
        };
        $request = new Request();
        $v = $sut->edit($request, 1);
        $this->assertSame($mockView, $v);
    }
}
