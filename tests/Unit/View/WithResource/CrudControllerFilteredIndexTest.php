<?php

declare(strict_types=1);

namespace Tests\Smorken\Controller\Unit\View\WithResource;

use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\View;
use Mockery as m;
use Smorken\Controller\Contracts\View\WithResource\WithFilteredRepository;
use Smorken\Controller\View\WithResource\CrudController;
use Smorken\Domain\Repositories\Contracts\FilteredRepository;
use Smorken\Domain\ViewModels\Contracts\FilteredViewModel;
use Smorken\QueryStringFilter\Contracts\QueryStringFilter;
use Tests\Smorken\Controller\Unit\WithMockeryTestCase;

class CrudControllerFilteredIndexTest extends WithMockeryTestCase
{
    public function test_it_can_use_a_filtered_repository(): void
    {
        View::shouldReceive('share')->once()->with('controller', m::type('string'));
        $mockView = m::mock(\Illuminate\Contracts\View\View::class);
        View::shouldReceive('make')->once()->with('index')->andReturn($mockView);
        $mockFilteredRepository = m::mock(FilteredRepository::class);
        $paginator = new Paginator(new Collection([]), 20);
        $mockFilteredRepository->shouldReceive('__invoke')
            ->once()
            ->with(m::type(QueryStringFilter::class), 20)
            ->andReturn($paginator);
        $mockView->shouldReceive('with')
            ->once()
            ->with('viewModel', m::type(FilteredViewModel::class))
            ->andReturnSelf();
        $sut = new class($mockFilteredRepository) extends CrudController implements WithFilteredRepository
        {
            public function __construct(FilteredRepository $filteredRepository)
            {
                $this->filteredRepository = $filteredRepository;
                parent::__construct();
            }
        };
        $v = $sut->index(new Request());
        $this->assertSame($mockView, $v);
    }
}
