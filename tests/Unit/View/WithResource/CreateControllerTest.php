<?php

declare(strict_types=1);

namespace Tests\Smorken\Controller\Unit\View\WithResource;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Mockery as m;
use Smorken\Controller\Contracts\View\WithResource\WithRepositoryFactory;
use Smorken\Controller\Contracts\View\WithResource\WithRetrieveRepository;
use Smorken\Controller\View\WithResource\CreateController;
use Smorken\Controller\View\WithResource\Exceptions\ResourceControllerException;
use Smorken\Domain\Factories\RepositoryFactory;
use Smorken\Domain\Repositories\Contracts\RetrieveRepository;
use Smorken\Domain\ViewModels\Contracts\RetrieveViewModel;
use Tests\Smorken\Controller\Unit\WithMockeryTestCase;

class CreateControllerTest extends WithMockeryTestCase
{
    public function test_it_can_use_a_repository_factory_using_default_method(): void
    {
        View::shouldReceive('share')->once()->with('controller', m::type('string'));
        $mockView = m::mock(\Illuminate\Contracts\View\View::class);
        View::shouldReceive('make')->once()->with('create')->andReturn($mockView);
        $mockRepositoryFactory = m::mock(RepositoryFactory::class);
        $model = m::mock(Model::class);
        $mockRepositoryFactory->shouldReceive('emptyModel')
            ->once()
            ->with()
            ->andReturn($model);
        $mockView->shouldReceive('with')
            ->once()
            ->with('viewModel', m::type(RetrieveViewModel::class))
            ->andReturnSelf();
        $sut = new class($mockRepositoryFactory) extends CreateController implements WithRepositoryFactory
        {
            public function __construct(RepositoryFactory $repositoryFactory)
            {
                $this->repositoryFactory = $repositoryFactory;
                parent::__construct();
            }
        };
        $v = $sut->create(new Request());
        $this->assertSame($mockView, $v);
    }

    public function test_it_can_use_a_retrieve_repository(): void
    {
        View::shouldReceive('share')->once()->with('controller', m::type('string'));
        $mockView = m::mock(\Illuminate\Contracts\View\View::class);
        View::shouldReceive('make')->once()->with('create')->andReturn($mockView);
        $mockRetrieveRepository = m::mock(RetrieveRepository::class);
        $model = m::mock(Model::class);
        $mockRetrieveRepository->shouldReceive('emptyModel')
            ->once()
            ->andReturn($model);
        $mockView->shouldReceive('with')
            ->once()
            ->with('viewModel', m::type(RetrieveViewModel::class))
            ->andReturnSelf();
        $sut = new class($mockRetrieveRepository) extends CreateController implements WithRetrieveRepository
        {
            public function __construct(RetrieveRepository $retrieveRepository)
            {
                $this->retrieveRepository = $retrieveRepository;
                parent::__construct();
            }
        };
        $v = $sut->create(new Request());
        $this->assertSame($mockView, $v);
    }

    public function test_it_should_throw_an_exception_without_a_model_retrieve_method(): void
    {
        View::shouldReceive('share')->once()->with('controller', CreateController::class);
        $mockView = m::mock(\Illuminate\Contracts\View\View::class);
        View::shouldReceive('make')->once()->with('create')->andReturn($mockView);
        $mockView->shouldReceive('with')
            ->never();
        $sut = new CreateController();
        $this->expectException(ResourceControllerException::class);
        $this->expectExceptionMessage('No create model retrieve method implemented');
        $sut->create(new Request());
    }
}
