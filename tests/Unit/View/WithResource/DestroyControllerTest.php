<?php

declare(strict_types=1);

namespace Tests\Smorken\Controller\Unit\View\WithResource;

use Illuminate\Contracts\Session\Session;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\View;
use Illuminate\Support\MessageBag;
use Mockery as m;
use Smorken\Controller\Contracts\View\WithResource\WithActionFactory;
use Smorken\Controller\Contracts\View\WithResource\WithDeleteAction;
use Smorken\Controller\View\WithResource\DestroyController;
use Smorken\Domain\Actions\Contracts\DeleteAction;
use Smorken\Domain\Actions\Results\DeleteResult;
use Smorken\Domain\Factories\ActionFactory;
use Tests\Smorken\Controller\Unit\WithMockeryTestCase;

class DestroyControllerTest extends WithMockeryTestCase
{
    public function test_it_can_delete_a_record_using_action_factory(): void
    {
        View::shouldReceive('share')->once()->with('controller', m::type('string'));
        $actionFactory = m::mock(ActionFactory::class);
        $actionFactory->shouldReceive('delete')
            ->once()
            ->with(1)
            ->andReturn(new DeleteResult(1, true, new MessageBag()));
        $request = new Request([]);
        $request->setLaravelSession(m::mock(Session::class));
        $request->session()->shouldReceive('flash')
            ->once()
            ->with('flash:success', 'Deleted resource [1].');
        $mockResponse = m::mock(RedirectResponse::class);
        Response::shouldReceive('redirectToAction')
            ->once()
            ->with(m::type('array'), [])
            ->andReturn($mockResponse);
        $sut = new class($actionFactory) extends DestroyController implements WithActionFactory
        {
            use \Smorken\Controller\View\WithResource\Concerns\WithActionFactory;

            public function __construct(ActionFactory $actionFactory)
            {
                parent::__construct();
                $this->actionFactory = $actionFactory;
            }
        };
        $this->assertSame($mockResponse, $sut->destroy($request, 1));
    }

    public function test_it_can_delete_a_record_using_delete_action(): void
    {
        View::shouldReceive('share')->once()->with('controller', m::type('string'));
        $deleteAction = m::mock(DeleteAction::class);
        $deleteAction->shouldReceive('__invoke')
            ->once()
            ->with(1)
            ->andReturn(new DeleteResult(1, true, new MessageBag()));
        $request = new Request([]);
        $request->setLaravelSession(m::mock(Session::class));
        $request->session()->shouldReceive('flash')
            ->once()
            ->with('flash:success', 'Deleted resource [1].');
        $mockResponse = m::mock(RedirectResponse::class);
        Response::shouldReceive('redirectToAction')
            ->once()
            ->with(m::type('array'), [])
            ->andReturn($mockResponse);
        $sut = new class($deleteAction) extends DestroyController implements WithDeleteAction
        {
            use \Smorken\Controller\View\WithResource\Concerns\WithDeleteAction;

            public function __construct(DeleteAction $deleteAction)
            {
                parent::__construct();
                $this->deleteAction = $deleteAction;
            }
        };
        $this->assertSame($mockResponse, $sut->destroy($request, 1));
    }

    public function test_it_can_fail_to_delete_a_record_using_delete_action(): void
    {
        View::shouldReceive('share')->once()->with('controller', m::type('string'));
        $deleteAction = m::mock(DeleteAction::class);
        $deleteAction->shouldReceive('__invoke')
            ->once()
            ->with(1)
            ->andReturn(new DeleteResult(1, false, new MessageBag(['foo' => 'bar'])));
        $request = new Request([]);
        $request->setLaravelSession(m::mock(Session::class));
        $request->session()->shouldReceive('flash')
            ->once()
            ->with('flash:danger', 'Error deleting resource [1].');
        $request->session()->shouldReceive('has')
            ->once()
            ->with('foo')
            ->andReturn(false);
        $request->session()->shouldReceive('flash')
            ->once()
            ->with('foo', 'bar');
        $mockResponse = m::mock(RedirectResponse::class);
        Response::shouldReceive('redirectToAction')
            ->once()
            ->with(m::type('array'), [])
            ->andReturn($mockResponse);
        $sut = new class($deleteAction) extends DestroyController implements WithDeleteAction
        {
            use \Smorken\Controller\View\WithResource\Concerns\WithDeleteAction;

            public function __construct(DeleteAction $deleteAction)
            {
                parent::__construct();
                $this->deleteAction = $deleteAction;
            }
        };
        $this->assertSame($mockResponse, $sut->destroy($request, 1));
    }
}
