<?php

declare(strict_types=1);

namespace Tests\Smorken\Controller\Unit\View\WithResource;

use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\View;
use Mockery as m;
use Smorken\Controller\Contracts\View\WithResource\WithFilteredRepository;
use Smorken\Controller\Contracts\View\WithResource\WithRepositoryFactory;
use Smorken\Controller\View\WithResource\Exceptions\ResourceControllerException;
use Smorken\Controller\View\WithResource\FilteredIndexController;
use Smorken\Domain\Factories\RepositoryFactory;
use Smorken\Domain\Repositories\Contracts\FilteredRepository;
use Smorken\Domain\ViewModels\Contracts\FilteredViewModel;
use Smorken\QueryStringFilter\Contracts\QueryStringFilter;
use Tests\Smorken\Controller\Unit\WithMockeryTestCase;

class FilteredIndexControllerTest extends WithMockeryTestCase
{
    public function test_it_can_use_a_filtered_repository(): void
    {
        View::shouldReceive('share')->once()->with('controller', m::type('string'));
        $mockView = m::mock(\Illuminate\Contracts\View\View::class);
        View::shouldReceive('make')->once()->with('index')->andReturn($mockView);
        $mockFilteredRepository = m::mock(FilteredRepository::class);
        $paginator = new Paginator(new Collection([]), 20);
        $mockFilteredRepository->shouldReceive('__invoke')
            ->once()
            ->with(m::type(QueryStringFilter::class), 20)
            ->andReturn($paginator);
        $mockView->shouldReceive('with')
            ->once()
            ->with('viewModel', m::type(FilteredViewModel::class))
            ->andReturnSelf();
        $sut = new class($mockFilteredRepository) extends FilteredIndexController implements WithFilteredRepository
        {
            public function __construct(FilteredRepository $filteredRepository)
            {
                $this->filteredRepository = $filteredRepository;
                parent::__construct();
            }
        };
        $v = $sut->index(new Request());
        $this->assertSame($mockView, $v);
    }

    public function test_it_can_use_a_repository_factory(): void
    {
        View::shouldReceive('share')->once()->with('controller', m::type('string'));
        $mockView = m::mock(\Illuminate\Contracts\View\View::class);
        View::shouldReceive('make')->once()->with('index')->andReturn($mockView);
        $mockRepositoryFactory = m::mock(RepositoryFactory::class);
        $paginator = new Paginator(new Collection([]), 20);
        $mockRepositoryFactory->shouldReceive('filtered')
            ->once()
            ->with(m::type(QueryStringFilter::class), 20)
            ->andReturn($paginator);
        $mockView->shouldReceive('with')
            ->once()
            ->with('viewModel', m::type(FilteredViewModel::class))
            ->andReturnSelf();
        $sut = new class($mockRepositoryFactory) extends FilteredIndexController implements WithRepositoryFactory
        {
            public function __construct(RepositoryFactory $repositoryFactory)
            {
                $this->repositoryFactory = $repositoryFactory;
                parent::__construct();
            }
        };
        $v = $sut->index(new Request());
        $this->assertSame($mockView, $v);
    }

    public function test_it_should_throw_an_exception_without_a_model_retrieve_method(): void
    {
        View::shouldReceive('share')->once()->with('controller', FilteredIndexController::class);
        $mockView = m::mock(\Illuminate\Contracts\View\View::class);
        View::shouldReceive('make')->once()->with('index')->andReturn($mockView);
        $sut = new FilteredIndexController();
        $this->expectException(ResourceControllerException::class);
        $this->expectExceptionMessage('No filtered models retrieve method implemented');
        $sut->index(new Request());
    }
}
