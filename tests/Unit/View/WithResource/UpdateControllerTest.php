<?php

declare(strict_types=1);

namespace Tests\Smorken\Controller\Unit\View\WithResource;

use Illuminate\Contracts\Session\Session;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\View;
use Illuminate\Support\MessageBag;
use Mockery as m;
use Smorken\Controller\Contracts\View\WithResource\WithActionFactory;
use Smorken\Controller\Contracts\View\WithResource\WithUpsertAction;
use Smorken\Controller\View\WithResource\UpdateController;
use Smorken\Domain\Actions\Contracts\Upsertable;
use Smorken\Domain\Actions\Contracts\UpsertAction;
use Smorken\Domain\Actions\Results\SaveResult;
use Smorken\Domain\Factories\ActionFactory;
use Smorken\Model\Contracts\Model;
use Tests\Smorken\Controller\Unit\WithMockeryTestCase;

class UpdateControllerTest extends WithMockeryTestCase
{
    public function test_it_can_fail_to_update_a_record_using_upsert_action(): void
    {
        View::shouldReceive('share')->once()->with('controller', m::type('string'));
        $upsertAction = m::mock(UpsertAction::class);
        $upsertAction->shouldReceive('__invoke')
            ->once()
            ->with(m::type(Upsertable::class))
            ->andReturn(new SaveResult(null, false, true, new MessageBag(['foo' => 'bar'])));
        $request = new Request([]);
        $request->setLaravelSession(m::mock(Session::class));
        $request->session()->shouldReceive('flash')
            ->once()
            ->with('flash:danger', 'Error saving resource [new].');
        $request->session()->shouldReceive('has')
            ->once()
            ->with('foo')
            ->andReturn(false);
        $request->session()->shouldReceive('flash')
            ->once()
            ->with('foo', 'bar');
        $mockResponse = m::mock(RedirectResponse::class);
        Response::shouldReceive('redirectToAction')
            ->once()
            ->with(m::type('array'), [])
            ->andReturn($mockResponse);
        $sut = new class($upsertAction) extends UpdateController implements WithUpsertAction
        {
            use \Smorken\Controller\View\WithResource\Concerns\WithUpsertAction;

            public function __construct(UpsertAction $upsertAction)
            {
                parent::__construct();
                $this->upsertAction = $upsertAction;
            }
        };
        $this->assertSame($mockResponse, $sut->update($request, 1));
    }

    public function test_it_can_update_a_record_using_action_factory(): void
    {
        View::shouldReceive('share')->once()->with('controller', m::type('string'));
        $actionFactory = m::mock(ActionFactory::class);
        $model = m::mock(Model::class);
        $model->shouldReceive('getKey')->andReturn(1);
        $actionFactory->shouldReceive('upsert')
            ->once()
            ->with(m::type(Upsertable::class))
            ->andReturn(new SaveResult($model, true, true, new MessageBag()));
        $request = new Request([]);
        $request->setLaravelSession(m::mock(Session::class));
        $request->session()->shouldReceive('flash')
            ->once()
            ->with('flash:success', 'Saved resource [1].');
        $mockResponse = m::mock(RedirectResponse::class);
        Response::shouldReceive('redirectToAction')
            ->once()
            ->with(m::type('array'), [])
            ->andReturn($mockResponse);
        $sut = new class($actionFactory) extends UpdateController implements WithActionFactory
        {
            use \Smorken\Controller\View\WithResource\Concerns\WithActionFactory;

            public function __construct(ActionFactory $actionFactory)
            {
                parent::__construct();
                $this->actionFactory = $actionFactory;
            }
        };
        $this->assertSame($mockResponse, $sut->update($request, 1));
    }

    public function test_it_can_update_a_record_using_upsert_action(): void
    {
        View::shouldReceive('share')->once()->with('controller', m::type('string'));
        $upsertAction = m::mock(UpsertAction::class);
        $model = m::mock(Model::class);
        $model->shouldReceive('getKey')->andReturn(1);
        $upsertAction->shouldReceive('__invoke')
            ->once()
            ->with(m::type(Upsertable::class))
            ->andReturn(new SaveResult($model, true, true, new MessageBag()));
        $request = new Request([]);
        $request->setLaravelSession(m::mock(Session::class));
        $request->session()->shouldReceive('flash')
            ->once()
            ->with('flash:success', 'Saved resource [1].');
        $mockResponse = m::mock(RedirectResponse::class);
        Response::shouldReceive('redirectToAction')
            ->once()
            ->with(m::type('array'), [])
            ->andReturn($mockResponse);
        $sut = new class($upsertAction) extends UpdateController implements WithUpsertAction
        {
            use \Smorken\Controller\View\WithResource\Concerns\WithUpsertAction;

            public function __construct(UpsertAction $upsertAction)
            {
                parent::__construct();
                $this->upsertAction = $upsertAction;
            }
        };
        $this->assertSame($mockResponse, $sut->update($request, 1));
    }
}
