<?php

declare(strict_types=1);

namespace Tests\Smorken\Controller\Unit\View\WithResource;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Mockery as m;
use Smorken\Controller\Contracts\View\WithResource\WithRetrieveRepository;
use Smorken\Controller\View\WithResource\CrudController;
use Smorken\Domain\Repositories\Contracts\RetrieveRepository;
use Smorken\Domain\ViewModels\Contracts\RetrieveViewModel;
use Tests\Smorken\Controller\Unit\WithMockeryTestCase;

class CrudControllerShowTest extends WithMockeryTestCase
{
    public function test_it_can_use_a_retrieve_repository(): void
    {
        View::shouldReceive('share')->once()->with('controller', m::type('string'));
        $mockView = m::mock(\Illuminate\Contracts\View\View::class);
        View::shouldReceive('make')->once()->with('show')->andReturn($mockView);
        $mockRetrieveRepository = m::mock(RetrieveRepository::class);
        $model = m::mock(Model::class);
        $mockRetrieveRepository->shouldReceive('__invoke')
            ->once()
            ->with(1)
            ->andReturn($model);
        $mockView->shouldReceive('with')
            ->once()
            ->with('viewModel', m::type(RetrieveViewModel::class))
            ->andReturnSelf();
        $sut = new class($mockRetrieveRepository) extends CrudController implements WithRetrieveRepository
        {
            public function __construct(RetrieveRepository $retrieveRepository)
            {
                $this->retrieveRepository = $retrieveRepository;
                parent::__construct();
            }
        };
        $request = new Request();
        $v = $sut->show($request, 1);
        $this->assertSame($mockView, $v);
    }
}
