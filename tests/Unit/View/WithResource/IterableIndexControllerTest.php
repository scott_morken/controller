<?php

declare(strict_types=1);

namespace Tests\Smorken\Controller\Unit\View\WithResource;

use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\View;
use Mockery as m;
use Smorken\Controller\Contracts\View\WithResource\WithIterableRepository;
use Smorken\Controller\Contracts\View\WithResource\WithRepositoryFactory;
use Smorken\Controller\View\WithResource\Exceptions\ResourceControllerException;
use Smorken\Controller\View\WithResource\IterableIndexController;
use Smorken\Domain\Factories\RepositoryFactory;
use Smorken\Domain\Repositories\Contracts\IterableRepository;
use Smorken\Domain\ViewModels\Contracts\IterableViewModel;
use Tests\Smorken\Controller\Unit\WithMockeryTestCase;

class IterableIndexControllerTest extends WithMockeryTestCase
{
    public function test_it_can_use_a_repository_factory_using_default_method(): void
    {
        View::shouldReceive('share')->once()->with('controller', m::type('string'));
        $mockView = m::mock(\Illuminate\Contracts\View\View::class);
        View::shouldReceive('make')->once()->with('index')->andReturn($mockView);
        $mockRepositoryFactory = m::mock(RepositoryFactory::class);
        $paginator = new Paginator(new Collection([]), 20);
        $mockRepositoryFactory->shouldReceive('get')
            ->once()
            ->with(20)
            ->andReturn($paginator);
        $mockView->shouldReceive('with')
            ->once()
            ->with('viewModel', m::type(IterableViewModel::class))
            ->andReturnSelf();
        $sut = new class($mockRepositoryFactory) extends IterableIndexController implements WithRepositoryFactory
        {
            public function __construct(RepositoryFactory $repositoryFactory)
            {
                $this->repositoryFactory = $repositoryFactory;
                parent::__construct();
            }
        };
        $v = $sut->index(new Request());
        $this->assertSame($mockView, $v);
    }

    public function test_it_can_use_a_repository_factory_using_overridden_method(): void
    {
        View::shouldReceive('share')->once()->with('controller', m::type('string'));
        $mockView = m::mock(\Illuminate\Contracts\View\View::class);
        View::shouldReceive('make')->once()->with('index')->andReturn($mockView);
        $mockRepositoryFactory = m::mock(RepositoryFactory::class);
        $paginator = new Paginator(new Collection([]), 20);
        $mockRepositoryFactory->shouldReceive('all')
            ->once()
            ->with()
            ->andReturn($paginator);
        $mockView->shouldReceive('with')
            ->once()
            ->with('viewModel', m::type(IterableViewModel::class))
            ->andReturnSelf();
        $sut = new class($mockRepositoryFactory) extends IterableIndexController implements WithRepositoryFactory
        {
            public function __construct(RepositoryFactory $repositoryFactory)
            {
                $this->repositoryFactory = $repositoryFactory;
                parent::__construct();
            }

            protected function getRepositoryFactoryMethod(): string
            {
                return 'all';
            }
        };
        $v = $sut->index(new Request());
        $this->assertSame($mockView, $v);
    }

    public function test_it_can_use_an_iterable_repository(): void
    {
        View::shouldReceive('share')->once()->with('controller', m::type('string'));
        $mockView = m::mock(\Illuminate\Contracts\View\View::class);
        View::shouldReceive('make')->once()->with('index')->andReturn($mockView);
        $mockIterableRepository = m::mock(IterableRepository::class);
        $paginator = new Paginator(new Collection([]), 20);
        $mockIterableRepository->shouldReceive('__invoke')
            ->once()
            ->with(20)
            ->andReturn($paginator);
        $mockView->shouldReceive('with')
            ->once()
            ->with('viewModel', m::type(IterableViewModel::class))
            ->andReturnSelf();
        $sut = new class($mockIterableRepository) extends IterableIndexController implements WithIterableRepository
        {
            public function __construct(IterableRepository $retrieveRepository)
            {
                $this->iterableRepository = $retrieveRepository;
                parent::__construct();
            }
        };
        $v = $sut->index(new Request());
        $this->assertSame($mockView, $v);
    }

    public function test_it_should_throw_an_exception_without_a_model_retrieve_method(): void
    {
        View::shouldReceive('share')->once()->with('controller', IterableIndexController::class);
        $mockView = m::mock(\Illuminate\Contracts\View\View::class);
        View::shouldReceive('make')->once()->with('index')->andReturn($mockView);
        $sut = new IterableIndexController();
        $this->expectException(ResourceControllerException::class);
        $this->expectExceptionMessage('No iterable models retrieve method implemented');
        $sut->index(new Request());
    }
}
