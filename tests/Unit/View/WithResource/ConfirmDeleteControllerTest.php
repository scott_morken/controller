<?php

declare(strict_types=1);

namespace Tests\Smorken\Controller\Unit\View\WithResource;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Mockery as m;
use Smorken\Controller\Contracts\View\WithResource\WithRepositoryFactory;
use Smorken\Controller\Contracts\View\WithResource\WithRetrieveRepository;
use Smorken\Controller\View\WithResource\ConfirmDeleteController;
use Smorken\Controller\View\WithResource\Exceptions\ResourceControllerException;
use Smorken\Domain\Factories\RepositoryFactory;
use Smorken\Domain\Repositories\Contracts\RetrieveRepository;
use Smorken\Domain\ViewModels\Contracts\RetrieveViewModel;
use Tests\Smorken\Controller\Unit\WithMockeryTestCase;

class ConfirmDeleteControllerTest extends WithMockeryTestCase
{
    public function test_it_can_use_a_repository_factory_using_default_method(): void
    {
        View::shouldReceive('share')->once()->with('controller', m::type('string'));
        $mockView = m::mock(\Illuminate\Contracts\View\View::class);
        View::shouldReceive('make')->once()->with('delete')->andReturn($mockView);
        $mockRepositoryFactory = m::mock(RepositoryFactory::class);
        $model = m::mock(Model::class);
        $mockRepositoryFactory->shouldReceive('find')
            ->once()
            ->with(1)
            ->andReturn($model);
        $mockView->shouldReceive('with')
            ->once()
            ->with('viewModel', m::type(RetrieveViewModel::class))
            ->andReturn($mockView);
        $sut = new class($mockRepositoryFactory) extends ConfirmDeleteController implements WithRepositoryFactory
        {
            public function __construct(RepositoryFactory $repositoryFactory)
            {
                $this->repositoryFactory = $repositoryFactory;
                parent::__construct();
            }
        };
        $v = $sut->confirmDelete(new Request(), 1);
        $this->assertSame($mockView, $v);
    }

    public function test_it_can_use_a_retrieve_repository(): void
    {
        View::shouldReceive('share')->once()->with('controller', m::type('string'));
        $mockView = m::mock(\Illuminate\Contracts\View\View::class);
        View::shouldReceive('make')->once()->with('delete')->andReturn($mockView);
        $mockRetrieveRepository = m::mock(RetrieveRepository::class);
        $model = m::mock(Model::class);
        $mockRetrieveRepository->shouldReceive('__invoke')
            ->once()
            ->with(1)
            ->andReturn($model);
        $mockView->shouldReceive('with')
            ->once()
            ->with('viewModel', m::type(RetrieveViewModel::class))
            ->andReturnSelf();
        $sut = new class($mockRetrieveRepository) extends ConfirmDeleteController implements WithRetrieveRepository
        {
            public function __construct(RetrieveRepository $retrieveRepository)
            {
                $this->retrieveRepository = $retrieveRepository;
                parent::__construct();
            }
        };
        $request = new Request();
        $v = $sut->confirmDelete($request, 1);
        $this->assertSame($mockView, $v);
    }

    public function test_it_should_throw_an_exception_without_a_model_retrieve_method(): void
    {
        View::shouldReceive('share')->once()->with('controller', ConfirmDeleteController::class);
        $mockView = m::mock(\Illuminate\Contracts\View\View::class);
        View::shouldReceive('make')->once()->with('delete')->andReturn($mockView);
        $mockView->shouldReceive('with')
            ->never();
        $sut = new ConfirmDeleteController();
        $this->expectException(ResourceControllerException::class);
        $this->expectExceptionMessage('No confirm delete model retrieve method implemented');
        $sut->confirmDelete(new Request(), 1);
    }
}
