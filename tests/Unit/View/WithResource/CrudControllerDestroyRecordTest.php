<?php

declare(strict_types=1);

namespace Tests\Smorken\Controller\Unit\View\WithResource;

use Illuminate\Contracts\Session\Session;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\View;
use Illuminate\Support\MessageBag;
use Mockery as m;
use Smorken\Controller\Contracts\View\WithResource\WithDeleteAction;
use Smorken\Controller\Contracts\View\WithResource\WithRetrieveRepository;
use Smorken\Controller\View\WithResource\CrudController;
use Smorken\Domain\Actions\Contracts\DeleteAction;
use Smorken\Domain\Actions\Results\DeleteResult;
use Smorken\Domain\Repositories\Contracts\RetrieveRepository;
use Smorken\Domain\ViewModels\Contracts\RetrieveViewModel;
use Tests\Smorken\Controller\Unit\WithMockeryTestCase;

class CrudControllerDestroyRecordTest extends WithMockeryTestCase
{
    public function test_it_can_delete_a_record_using_delete_action(): void
    {
        View::shouldReceive('share')->once()->with('controller', m::type('string'));
        $deleteAction = m::mock(DeleteAction::class);
        $deleteAction->shouldReceive('__invoke')
            ->once()
            ->with(1)
            ->andReturn(new DeleteResult(1, true, new MessageBag()));
        $request = new Request([]);
        $request->setLaravelSession(m::mock(Session::class));
        $request->session()->shouldReceive('flash')
            ->once()
            ->with('flash:success', 'Deleted resource [1].');
        $mockResponse = m::mock(RedirectResponse::class);
        Response::shouldReceive('redirectToAction')
            ->once()
            ->with(m::type('array'), [])
            ->andReturn($mockResponse);
        $sut = new class($deleteAction) extends CrudController implements WithDeleteAction
        {
            use \Smorken\Controller\View\WithResource\Concerns\WithDeleteAction;

            public function __construct(DeleteAction $deleteAction)
            {
                parent::__construct();
                $this->deleteAction = $deleteAction;
            }
        };
        $this->assertSame($mockResponse, $sut->destroy($request, 1));
    }

    public function test_it_can_use_a_retrieve_repository(): void
    {
        View::shouldReceive('share')->once()->with('controller', m::type('string'));
        $mockView = m::mock(\Illuminate\Contracts\View\View::class);
        View::shouldReceive('make')->once()->with('delete')->andReturn($mockView);
        $mockRetrieveRepository = m::mock(RetrieveRepository::class);
        $model = m::mock(Model::class);
        $mockRetrieveRepository->shouldReceive('__invoke')
            ->once()
            ->with(1)
            ->andReturn($model);
        $mockView->shouldReceive('with')
            ->once()
            ->with('viewModel', m::type(RetrieveViewModel::class))
            ->andReturnSelf();
        $sut = new class($mockRetrieveRepository) extends CrudController implements WithRetrieveRepository
        {
            public function __construct(RetrieveRepository $retrieveRepository)
            {
                $this->retrieveRepository = $retrieveRepository;
                parent::__construct();
            }
        };
        $request = new Request();
        $v = $sut->confirmDelete($request, 1);
        $this->assertSame($mockView, $v);
    }
}
