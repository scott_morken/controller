<?php

namespace Tests\Smorken\Controller\Stubs\View\WithServices;

use Smorken\Controller\View\Controller;
use Smorken\Controller\View\WithService\Concerns\HasIndex;
use Smorken\Service\Contracts\Services\PaginateByFilterService;

class IndexPaginatedControllerStub extends Controller
{
    use HasIndex;

    protected string $baseView = 'foo';

    public function __construct(PaginateByFilterService $indexService)
    {
        $this->setIndexService($indexService);
        parent::__construct();
    }
}
